import math

def volumen_esfera(r):
        """
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera : Ing. Computadores 
Programa: volumen_esfera(r)
Función : Calcular volumen de una esfera
Autores : Gerald Salazar Elizondo(2018099115) e Brayan Rodriguez Villalobos (2018079212)
Version : 1.0
Entradas : radio
Restricciones: 0 < r
Salidas: volumen de la esfera
******************************************************************"""
        if r > 0:
                volumen = (4/3)*math.pi*r**3
                return volumen
        else:
                return "Datos incorrectos"

def volumen_p_base_cuadrada(h, l):
        """
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera : Ing. Computadores 
Programa: volumen_piramide(r)
Función : Calcular volumen de una piramide
Autores : Gerald Salazar Elizondo(2018099115) e Brayan Rodriguez Villalobos (2018079212)
Version : 1.0
Entradas : radio
Restricciones: 0 < r
Salidas: volumen de la esfera
******************************************************************"""
        if h > 0 and l > 0:
                area_base = l*l
                volumen = area_base * (1/3*h)
                return volumen
        else:
                "Datos incorrectos"

def area_esfera(Radio):
    """ Instituto Tecnológico de Costa Rica
        Ingeniería en COmputradores
        Programa:area_esfera
        Función: Calcula el área de una esfera
        Autores: Gerald Salazar Elizondo(2018099115) y Brayan Rodríguez Villalobos
        Versión:1.0
        Entradas: Radio de una esfera
        Restricciones: Debe ser un número positivo
        Salidas: Aréa de una esfera
        Fecha:26/4/18
        """
    
    Area=4*math.pi*Radio**2#Area
    return Area


def area_piramide(Lado,Apotema):
    """ Instituto Tecnológico de Costa Rica
        Ingeniería en COmputradores
        Programa:area_piramide
        Función: Calcula el área de una piramide
        Autores: Gerald Salazar Elizondo(2018099115) y Brayan Rodríguez Villalobos
        Versión:1.0
        Entradas: Lado de la base de una piramide cuadrangular, y la apotema de esta
        Restricciones: Debe ser números positivos
        Salidas: Aréa de una piramide
        Fecha:26/4/18
        """
        Area=Lado*(2*Apotema+Lado)#Area
        return Area

def volumen_cilindro(Radio,Altura):
    """ Instituto Tecnológico de Costa Rica
        Ingeniería en COmputradores
        Programa:volumen_cilindro
        Función: Calcula el volumen de un cilindro
        Autores: Gerald Salazar Elizondo(2018099115) y Brayan Rodríguez Villalobos
        Versión:1.0
        Entradas: Radio y Altura
        Restricciones: Debe ser números positivos
        Salidas: Volumen del cilindro
        Fecha:26/4/18
        """
    Volumen=(math.pi*Radio**2*Altura)
    return Volumen


